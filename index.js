
async function createOne(db, index) {
    await db.command(index);
}

async function deleteAll(db) {
    const collections = await db.listCollections({}, { nameOnly: true }).toArray();
    for (const collection of collections) {
        let res = await db.collection(collection.name).dropIndexes();
    }
}

module.exports = {
    createOne,
    deleteAll
}