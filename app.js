require('dotenv').config();
const db_uri = process.env.DB_URI;
const db_name = process.env.DB_NAME;
const input_dir = process.env.INPUT;
const index_file = process.env.INDEX;
const {MongoClient, FindCursor} = require('mongodb');
const client = new MongoClient(db_uri, {useUnifiedTopology: true});
const {ObjectId} = require('mongodb');
const utils = require('./utils.js');
const index = require('./index.js');



async function run() {
    //Get input
    const queries = await utils.getAllQueries(input_dir);
    const indexes = await utils.getAllIndexes(index_file);

    //Connect to MongoDB Database
    try {
        await client.connect();
        console.log("Connected to DB", db_name);
        const db = client.db(db_name);

        //Drop previous indexes
       await index.deleteAll(db);

        //Run queries
        for (const query of queries) {
            let aggregation_name = query["aggregate"];

            console.log('\n * Checking query', aggregation_name)

            //Create one index
            for (const id of indexes) {

                const index_name = id["indexes"][0]["name"];
                console.log('\n - Creating index', index_name)
                await index.createOne(db, id);

                //Explain
                console.log('   Checking explain ...')
                const query_collection = db.collection(query["collection"]);
                const pipeline = query["pipeline"];
                const exp_start = process.hrtime();
                const exp = await explainQuery(query_collection, pipeline);
                const exp_duration = utils.logTimer(exp_start);
                const exp_usage = await parseExplain(exp, index_name);
                console.log('\t ... explain result:', exp_usage)
                console.log("Explain: ", JSON.stringify(exp, null, 2));

                console.log('   Checking stats ...')
                const id_start = process.hrtime();
                const query_res = await query_collection.aggregate(pipeline).toArray();
                // console.log("Query Results:", JSON.stringify(query_res).substr(0, 100), '...', query_res.length);
                const index_stats = await query_collection.aggregate([{'$indexStats':{}}]).toArray();
                const id_duration = utils.logTimer(id_start);
                const index_usage = await parseIndexStats(index_stats, index_name);
                console.log('\t ... stats result:', index_usage)

                // console.log("Index stats:", JSON.stringify(index_stats));

                //Logging results in report file 
                utils.createReport(aggregation_name, index_name, exp_usage, index_usage, exp_duration, id_duration);

                await index.deleteAll(db);

            }
        }
    } finally {
        await client.close();
        console.log("Done!")
    }
}

async function explainQuery(collection, pipeline) {

    const exp = await collection.aggregate(pipeline).explain();

    return exp;
}

async function parseExplain(exp, index_name) {
    let exp_usage;
    //If exp has not an array of stages catch error
    let winningPlan;
    try {
        winningPlan = await exp.stages[0].$cursor.queryPlanner.winningPlan;
    } catch {
        winningPlan = await exp.queryPlanner.winningPlan;
    }
    const used_index = await findIndex(winningPlan, index_name);
   
    if(typeof used_index === "undefined") {
        exp_usage = false;
    } else {
        exp_usage = used_index === index_name;
    }

    return exp_usage;
}

async function findIndex(winningPlan, index_name) {
    if(winningPlan.stage !== "IXSCAN") {
        const inputStage = winningPlan.inputStage;
        if(typeof inputStage === "undefined") {
            return inputStage;//return undefined
        } else {
            return findIndex(inputStage, index_name);
        }
    } else{
        return winningPlan.indexName;
    }
}

async function parseIndexStats(index_stats, index_name) {
    for(const index_stat of index_stats) {
        if(index_stat["name"] === index_name) {index_usage = index_stat["accesses"]["ops"];}
        
    }
    console.log(index_usage)
    return index_usage>0;
}
run()