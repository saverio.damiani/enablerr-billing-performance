const {ObjectId} = require('mongodb');

let values = {
    "{$EMPLOYEE}": new ObjectId("6288efb7f5c71f1348ca36d6"),
    "{$START_DATE}": new Date('Mon, 13 Dec 2021 00:00:00 GMT'), 
    "{$END_DATE}": new Date('Mon, 20 Dec 2021 23:00:00 GMT')
  }

  exports.values = values;